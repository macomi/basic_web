"use strict";

const gulp = require("gulp");

// Plugins
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const pump = require("pump"); // para errores entenderlos mejor.
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const htmlmin = require('gulp-htmlmin');
const cssnano = require("gulp-cssnano");
const merge = require("merge-stream");
const imagemin = require("gulp-imagemin");

// sass
// gulp.task("sass", function() {
//   var sassStream = gulp
//     .src('src/sass/main.sass')
//     .pipe(src/sass().on("error", sass.logError));

//     // merge styles
//     return merge(sassStream)
//         .pipe(concat('style.css'))
//         .pipe(gulp.dest('app/css'));
// });

// Static Server + watching scss/html files
gulp.task("serve", ["sass"], () => {
  browserSync.init({
    server: "./app"
  });

  gulp.watch("src/sass/**/*.sass", ["sass"]);
  gulp.watch("src/js/**/*.js", ["uglify_JS"]);
  gulp.watch("app/**/*.html").on("change", browserSync.reload);
  gulp.watch("src/**/*.html", ["minify_html"]);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task("sass", () => {
  return gulp
    .src("src/sass/main.sass")
    .pipe(sass().on("error", sass.logError))
    .pipe(concat("style.css"))
    .pipe(cssnano())
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("app/css"))
    .pipe(browserSync.stream());
});

// uglify jacaScript & auto-inject into browsers
gulp.task("uglify_JS", cb => {
  pump([gulp.src("src/js/**/*.js"), uglify(), gulp.dest("app/js")], cb).pipe(
    browserSync.stream()
  );
});

// Minify PNG, JPEG, GIF and SVG images
gulp.task("optimizar_img", () =>
  gulp
    .src("src/img/*")
    .pipe(imagemin())
    .pipe(gulp.dest("app/img"))
);

// minify html
gulp.task('minify_html', () => {
  return gulp.src('src/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app'));
});

// default init
gulp.task("default", ["serve"]);
